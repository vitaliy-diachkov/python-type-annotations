"""
Generic type hinting in Python.

Here is the example on how to annotate generic functions, for example,
for converting one objects into other.
"""
from abc import ABC, abstractmethod
from collections.abc import Iterable
from dataclasses import dataclass
from typing import List, TypeVar, Union


T = TypeVar('T')
K = TypeVar('K')


class BaseConverter(ABC):
    """
    Base converter class.

    Converters are used to cast one object to another. For example, extract and
    pop some data from complex objects to create simplier one.
    """

    def convert(self, obj: Union[T, Iterable[T]]) -> Union[K, List[K]]:
        if isinstance(obj, Iterable):
            return [self.convert_one(item) for item in obj]
        return self.convert_one(obj)

    @abstractmethod
    def convert_one(self, obj):
        """
        Previosly I wanted this method to look like this:

        >>> @abstractmethod
        ... def convert_one(self, obj: T) -> K:
        ...     pass

        But, when I tried to implement this method in BaseConverter subclasses
        I got and error from MyPy saying: "Imcompatible overrides" as doing
        next (i.e. overriding type annotatins in inherited classes):

        >>> def convert_one(self, obj: TypeA) -> TypeB:
        ...     pass

        Violates the Liskov substitution principle.

        So, in order to be able to specify type hints in child classes we
        should not set it in parent class.

        Another solution is to set the type annotation at the parent class
        and do not override it in child classes, but we want type annotations
        in childclasses in order to make our IDE/text editors be able to
        autocomplete from context.
        """
        pass


@dataclass
class User:
    id: int
    name: str


@dataclass
class ExtendedUser:
    id: int
    first_name: str
    last_name: str


class ExtendedUserToUserConverter(BaseConverter):
    def convert_one(self, obj: ExtendedUser) -> User:
        return User(
            id=obj.id,
            name=f'{obj.first_name} {obj.last_name}'
        )


if __name__ == '__main__':
    extended_user = ExtendedUser(
        id=1,
        first_name='Vitaliy',
        last_name='Diachkov'
    )
    converter = ExtendedUserToUserConverter()
    user = converter.convert(extended_user)
    assert isinstance(user, User), 'user is not instance of User'
